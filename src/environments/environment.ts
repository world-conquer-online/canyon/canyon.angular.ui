export const environment = {
  isProduction: true,

  url: 'https://localhost:4200',

  backend: {
    url: 'http://localhost:5000'
  },

  sso: {
    url: 'http://localhost:8080',
    realm: 'mysql-provider-test',
    clientId: 'test-client'
  }
};
