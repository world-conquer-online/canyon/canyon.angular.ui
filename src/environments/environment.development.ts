export const environment = {
  isProduction: false,

  url: 'https://localhost:4200',

  backend: {
    url: 'https://localhost:7006'
  },

  sso: {
    url: 'http://localhost:8080',
    realm: 'master',
    clientId: 'world-conquer-local'
  }
};
