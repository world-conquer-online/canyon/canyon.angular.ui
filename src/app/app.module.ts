import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  NgbAccordionModule,
  NgbCarouselModule,
  NgbDropdownModule,
  NgbModule,
  NgbToastModule
} from '@ng-bootstrap/ng-bootstrap';
import { IndexComponent } from './pages/index/index.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { SignComponent } from './pages/sign/sign.component';
import { PartnersComponent } from './components/partners/partners.component';
import { ToastsContainerComponent } from './components/toasts-container/toasts-container.component';
import { SignInCallbackComponent } from './components/callbacks/sign-in-callback/sign-in-callback.component';
import {CallbacksModule} from "./components/callbacks/callbacks-module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {HttpRequestService} from "./interceptors/http-request.service";
import {UserModule} from "./pages/user/user.module";
import { DownloadClientsComponent } from './pages/download/clients/download-clients.component';
import { DownloadPatchesComponent } from './pages/download/patches/download-patches.component';
import {DownloadsModule} from "./pages/download/downloads.module";
import {RankingsModule} from "./pages/rankings/rankings.module";

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    RegistrationComponent,
    SignComponent,
    PartnersComponent,
    ToastsContainerComponent,
    SignInCallbackComponent,
    DownloadClientsComponent,
    DownloadPatchesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,

    AppRoutingModule,
    CallbacksModule,
    UserModule,
    DownloadsModule,
    RankingsModule,

    NgbModule,
    NgbCarouselModule,
    NgbAccordionModule,
    NgbDropdownModule,
    NgbToastModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
