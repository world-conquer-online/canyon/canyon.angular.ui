// I highly recommend to change this method and use a BFF so your front-end does not store your
// access tokens.
// I ain't coding a BFF now, but this may be changed at a later time.
export interface TokenStorageModel {
  accessToken: string;
  refreshToken: string;
  refreshTime: number;
  expirationTime: number;
  expirationSeconds: number;
}
