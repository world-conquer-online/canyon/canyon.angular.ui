export const SUCCESS_TOAST_CLASS = 'bg-success text-light';
export const DANGER_TOAST_CLASS = 'bg-danger text-light';
export const WARNING_TOAST_CLASS = 'bg-warning text-light';
export const INFO_TOAST_CLASS = 'bg-primary text-light';

export interface ToastModel {
  content: string;
  delay?: number;
  className?: string;
}
