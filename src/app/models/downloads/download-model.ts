export interface DownloadModel {
  name: string;
  hash: string;
  hashType: string;
  url: string;
}
