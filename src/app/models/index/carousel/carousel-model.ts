export interface CarouselModel {
  path: string;
  title?: string;
  description?: string;
  link?: string;
}
