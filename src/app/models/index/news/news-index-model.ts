export interface NewsIndexModel {
  category: string;
  title: string;
  content: string;
  image?: string;
  author: string;
  date: Date;
}
