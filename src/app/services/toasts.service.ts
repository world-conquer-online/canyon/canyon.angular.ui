import {Injectable} from '@angular/core';
import {ToastModel} from "../models/toasts/ToastModel";

@Injectable({
  providedIn: 'root'
})
export class ToastsService {
  toasts: ToastModel[] = [];
  constructor() { }

  show(content: string, options: any = {}) {
    this.toasts.push({ content, ...options });
  }

  remove(toast: any) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }

  clear() {
    this.toasts.splice(0, this.toasts.length);
  }
}
