import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

const defaultCacheExpirationMinutes = 60;
const expirationFactor = 60 * 1000;

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  constructor() {
    // nothing
  }

  public static save(key: string, value: any, expiration?: number) {
    this.saveFn({
      key: key,
      value: value,
      expirationMinutes: expiration
    });
  }

  private static saveFn(options: LocalStorageSaveOptions): void {
    options.expirationMinutes = options.expirationMinutes || defaultCacheExpirationMinutes;
    const expiration = options.expirationMinutes * expirationFactor;
    const record: LocalStorageItem = {
      value: typeof options.value === 'string' ? options.value : JSON.stringify(options.value),
      expiration: new Date().getTime() + expiration,
      hasExpiration: expiration !== 0,
      type: typeof options.value
    };
    localStorage.setItem(options.key, JSON.stringify(record));
  }

  public static find<T>(key: string): T | null {
    const item = localStorage.getItem(key);
    if (item !== null) {
      const record = JSON.parse(item) as LocalStorageItem;
      const now = new Date().getTime();
      if (!record) {
        return null;
      }
      if (record.hasExpiration && record.expiration <= now) {
        this.remove(key);
        return null;
      }
      if (record.type === 'string') {
        return record.value as any as T; // T must be string
      }
      return JSON.parse(record.value) as T;
    }
    return null;
  }

  public static remove(key: string): void {
    localStorage.removeItem(key);
  }

  public static cleanLocalStorage(): void {
    localStorage.clear();
  }
}

export interface LocalStorageSaveOptions {
  key: string;
  value: any;
  expirationMinutes?: number;
}

export interface LocalStorageItem {
  value: string;
  expiration: number;
  hasExpiration: boolean;
  type: string;
}
