import { TestBed } from '@angular/core/testing';

import { CacheService } from './cache.service';

describe('CacheService', () => {
  let service: CacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save, get and remove string value', () => {
    const KEY = 'stringKey';
    CacheService.save(KEY, 'test value', 15);
    expect(CacheService.find<string>(KEY)).not.toBeNull();
    CacheService.remove(KEY);
    expect(CacheService.find<string>(KEY)).toBeNull();
  });

  it('should save, get and remove object value', () => {
    const KEY = 'objectKey';
    const VALUE = {text: 'test value'};
    CacheService.save(KEY, VALUE, 15);
    expect(CacheService.find<any>(KEY)).toEqual(VALUE);
    CacheService.remove(KEY);
    expect(CacheService.find<any>(KEY)).toBeNull();
  });
});
