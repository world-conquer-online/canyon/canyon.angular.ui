import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable, of} from "rxjs";
import {CacheService} from "./cache.service";
import {ToastsService} from "./toasts.service";
import {DANGER_TOAST_CLASS} from "../models/toasts/ToastModel";
import {TokenStorageModel} from "../models/authentication/token-storage-model";
import {isNullOrEmpty} from "../helpers/string.helper";

export const SESSION_STATE_CACHE_KEY = 'SessionState';
export const AUTHENTICATION_CACHE_KEY = 'AuthenticationData';
export const POST_AUTHENTICATION_URL_CACHE_KEY = 'PostAuthenticationRedirectUrl';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private toast: ToastsService
  ) { }

  authenticate(code: string, sessionState: string): Observable<any> {
    const currentSessionState = CacheService.find<string>(SESSION_STATE_CACHE_KEY);
    if (!currentSessionState) {
      this.toast.show('You do not have a session state.', { className: DANGER_TOAST_CLASS });
      return of();
    }

    if (currentSessionState !== sessionState) {
      this.toast.show('Your session state is invalid for this operation.', { className: DANGER_TOAST_CLASS });
      return of();
    }

    const url = environment.backend.url;
    return this.httpClient.post<any>(`${url}/api/authentication/sign-in`, {
      code,
      sessionState
    });
  }

  isAuthenticated(): boolean {
    const record = CacheService.find<TokenStorageModel>(AUTHENTICATION_CACHE_KEY);
    if (record == null) {
      return false;
    }
    return !isNullOrEmpty(record.accessToken) && record.expirationTime <= Date.now();
  }
}
