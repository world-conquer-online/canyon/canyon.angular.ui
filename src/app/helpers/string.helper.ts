export function isNullOrEmpty(value: string | undefined | null) {
  if (typeof value === 'undefined') {
    return true;
  }
  return value == null;
}
