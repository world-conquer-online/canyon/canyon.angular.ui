import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {SignInCallbackComponent} from "./sign-in-callback/sign-in-callback.component";

const routes: Routes = [
  {
    path: 'callback/sign-in',
    component: SignInCallbackComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallbacksRoutingModule { }
