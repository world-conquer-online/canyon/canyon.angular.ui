import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CallbacksRoutingModule} from "./callbacks-routing-module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CallbacksRoutingModule
  ]
})
export class CallbacksModule { }
