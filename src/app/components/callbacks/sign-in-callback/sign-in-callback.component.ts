import {Component, OnInit} from '@angular/core';
import {AUTHENTICATION_CACHE_KEY, AuthenticationService} from "../../../services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {isNullOrEmpty} from "../../../helpers/string.helper";
import {ToastsService} from "../../../services/toasts.service";
import {DANGER_TOAST_CLASS, WARNING_TOAST_CLASS} from "../../../models/toasts/ToastModel";
import {CacheService} from "../../../services/cache.service";
import {TokenStorageModel} from "../../../models/authentication/token-storage-model";

@Component({
  selector: 'app-sign-in-callback',
  templateUrl: './sign-in-callback.component.html',
  styleUrls: ['./sign-in-callback.component.scss']
})
export class SignInCallbackComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toast: ToastsService) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((queryParams) => {
      if (!queryParams) {
        this.toast.show('Your authentication response is empty.', {
          className: WARNING_TOAST_CLASS
        });
        CacheService.cleanLocalStorage();
        return;
      }

      const code = queryParams['code'];
      const state = queryParams['state'];
      if (isNullOrEmpty(code) || isNullOrEmpty(state)) {
        this.navigateAndDisplayToast('Your authentication response is invalid.', WARNING_TOAST_CLASS, '/');
        CacheService.cleanLocalStorage();
        return;
      }

      this.authenticationService.authenticate(code, state).subscribe((result) => {
        if (!result.access_token) {
          this.navigateAndDisplayToast('Authentication attempt failed.', DANGER_TOAST_CLASS, '/');
          CacheService.cleanLocalStorage();
          return;
        }

        const refreshTime = Date.now() + result.expires_in * 0.75;
        const expirationTime = Date.now() + result.expires_in;
        const authenticationData: TokenStorageModel = {
          accessToken: result.access_token,
          refreshToken: result.refresh_token,
          refreshTime: refreshTime,
          expirationTime: expirationTime,
          expirationSeconds: result.expires_in
        };

        CacheService.save(AUTHENTICATION_CACHE_KEY, authenticationData, result.expires_in);
        this.router.navigateByUrl('/user/game/register');
      });
    });
  }

  private navigateAndDisplayToast(message: string, className: string, url: string) {
    this.router.navigateByUrl(url).then(() => {
      this.toast.show(message, {
        className,
        delay: 10000
      })
    });
  }
}
