import { Component } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {CacheService} from "../../services/cache.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(public authenticationService: AuthenticationService) {
  }

  logout() {
    CacheService.cleanLocalStorage();
  }
}
