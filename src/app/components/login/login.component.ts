import { Component } from '@angular/core';
import {environment} from "../../../environments/environment";
import {CacheService} from "../../services/cache.service";
import {SESSION_STATE_CACHE_KEY} from "../../services/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  sessionState: string;

  constructor() {
    this.sessionState = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    CacheService.remove(SESSION_STATE_CACHE_KEY);
    CacheService.save(SESSION_STATE_CACHE_KEY, this.sessionState, 15);
  }

  getLoginUrl(): string {
    return `${environment.sso.url}/realms/${environment.sso.realm}/protocol/openid-connect`
      + `/auth?client_id=${environment.sso.clientId}&redirect_uri=${environment.url}/callback/sign-in&response_type=code`
      + `&state=${this.sessionState}`;
  }
}
