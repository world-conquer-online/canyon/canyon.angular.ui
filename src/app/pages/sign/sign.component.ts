import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {isNullOrEmpty} from "../../helpers/string.helper";

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.scss']
})
export class SignComponent implements OnInit {

  activeTab: any = 1;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.fragment.subscribe((fragment : string | null) => {
      if (!isNullOrEmpty(fragment) && fragment?.toLowerCase() === 'register') {
        this.activeTab = 2;
      }
      else {
        this.activeTab = 1;
      }
    });
  }
}
