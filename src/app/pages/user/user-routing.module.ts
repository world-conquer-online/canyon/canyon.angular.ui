import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {GameRegistrationComponent} from "./game-registration/game-registration.component";

const routes: Routes = [
  {
    path: 'user',
    children: [
      { path: 'game/register', component: GameRegistrationComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
