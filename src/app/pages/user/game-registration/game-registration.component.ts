import { Component } from '@angular/core';

interface TempAccount {
  id: number;
  userName: string;
  isBanned: boolean;
  banExpireTime?: Date;
}

const tempAccounts: TempAccount[] = [
  {
    id: 1,
    userName: 'felipe',
    isBanned: false
  },
  {
    id: 2,
    userName: 'felipe_banned',
    isBanned: true,
    banExpireTime: new Date('2024-03-01')
  },
  {
    id: 3,
    userName: 'felipe_perma_ban',
    isBanned: true
  },
];

@Component({
  selector: 'app-game-registration',
  templateUrl: './game-registration.component.html',
  styleUrls: ['./game-registration.component.scss']
})
export class GameRegistrationComponent {

  totalAccounts = tempAccounts.length;
  accounts: TempAccount[] = [];
  page = 1;
  indexesPerPage = 10;

  constructor() {
    this.refreshAccounts();
  }

  refreshAccounts() {
    this.accounts = tempAccounts.map((account, i) => (account)).slice(
      (this.page - 1) * this.indexesPerPage,
      (this.page - 1) * this.indexesPerPage + this.indexesPerPage,
    );
  }
}
