import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UserRoutingModule} from "./user-routing.module";
import {GameRegistrationComponent} from './game-registration/game-registration.component';
import {NgbPagination} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    GameRegistrationComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgbPagination,
    FormsModule
  ]
})
export class UserModule {
}
