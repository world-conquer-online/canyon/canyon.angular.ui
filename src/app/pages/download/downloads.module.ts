import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DownloadsRouterModule} from "./downloads-router.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DownloadsRouterModule
  ]
})
export class DownloadsModule {}
