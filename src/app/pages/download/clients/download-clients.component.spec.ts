import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadClientsComponent } from './download-clients.component';

describe('ClientsComponent', () => {
  let component: DownloadClientsComponent;
  let fixture: ComponentFixture<DownloadClientsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadClientsComponent]
    });
    fixture = TestBed.createComponent(DownloadClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
