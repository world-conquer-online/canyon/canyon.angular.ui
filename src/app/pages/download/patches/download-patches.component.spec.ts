import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadPatchesComponent } from './download-patches.component';

describe('PatchesComponent', () => {
  let component: DownloadPatchesComponent;
  let fixture: ComponentFixture<DownloadPatchesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadPatchesComponent]
    });
    fixture = TestBed.createComponent(DownloadPatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
