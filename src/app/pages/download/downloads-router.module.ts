import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {DownloadClientsComponent} from "./clients/download-clients.component";
import {DownloadPatchesComponent} from "./patches/download-patches.component";

const routes: Routes = [
  {
    path: 'download',
    children: [
      { path: 'client', component: DownloadClientsComponent },
      { path: 'patch', component: DownloadPatchesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DownloadsRouterModule {}
