import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingSupermanComponent } from './ranking-superman.component';

describe('RankingSupermanComponent', () => {
  let component: RankingSupermanComponent;
  let fixture: ComponentFixture<RankingSupermanComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RankingSupermanComponent]
    });
    fixture = TestBed.createComponent(RankingSupermanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
