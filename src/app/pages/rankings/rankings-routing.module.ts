import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RankingNobilityComponent} from "./ranking-nobility/ranking-nobility.component";
import {RankingSyndicateComponent} from "./ranking-syndicate/ranking-syndicate.component";
import {RankingSupermanComponent} from "./ranking-superman/ranking-superman.component";
import {RankingProfessionComponent} from "./ranking-profession/ranking-profession.component";
import {RankingPlayersComponent} from "./ranking-players/ranking-players.component";

const routes: Routes = [
  {
    path: 'ranking',
    children: [
      { path: 'player', component: RankingPlayersComponent },
      { path: 'profession/:profession', component: RankingProfessionComponent },
      { path: 'superman', component: RankingSupermanComponent },
      { path: 'syndicate', component: RankingSyndicateComponent },
      { path: 'nobility', component: RankingNobilityComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RankingsRoutingModule { }
