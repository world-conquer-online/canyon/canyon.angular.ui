import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingNobilityComponent } from './ranking-nobility.component';

describe('RankingNobilityComponent', () => {
  let component: RankingNobilityComponent;
  let fixture: ComponentFixture<RankingNobilityComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RankingNobilityComponent]
    });
    fixture = TestBed.createComponent(RankingNobilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
