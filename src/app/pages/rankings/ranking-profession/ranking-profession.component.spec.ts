import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingProfessionComponent } from './ranking-profession.component';

describe('RankingProfessionComponent', () => {
  let component: RankingProfessionComponent;
  let fixture: ComponentFixture<RankingProfessionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RankingProfessionComponent]
    });
    fixture = TestBed.createComponent(RankingProfessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
