import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RankingsRoutingModule } from './rankings-routing.module';
import { RankingPlayersComponent } from './ranking-players/ranking-players.component';
import { RankingProfessionComponent } from './ranking-profession/ranking-profession.component';
import { RankingSyndicateComponent } from './ranking-syndicate/ranking-syndicate.component';
import { RankingSupermanComponent } from './ranking-superman/ranking-superman.component';
import { RankingNobilityComponent } from './ranking-nobility/ranking-nobility.component';


@NgModule({
  declarations: [
    RankingPlayersComponent,
    RankingProfessionComponent,
    RankingSyndicateComponent,
    RankingSupermanComponent,
    RankingNobilityComponent
  ],
  imports: [
    CommonModule,
    RankingsRoutingModule
  ]
})
export class RankingsModule { }
