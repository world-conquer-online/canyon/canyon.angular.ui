import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingSyndicateComponent } from './ranking-syndicate.component';

describe('RankingSyndicateComponent', () => {
  let component: RankingSyndicateComponent;
  let fixture: ComponentFixture<RankingSyndicateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RankingSyndicateComponent]
    });
    fixture = TestBed.createComponent(RankingSyndicateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
