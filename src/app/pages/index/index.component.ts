import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CarouselModel} from "../../models/index/carousel/carousel-model";
import {NewsIndexModel} from "../../models/index/news/news-index-model";
import {DomSanitizer} from "@angular/platform-browser";
import {ToastsService} from "../../services/toasts.service";
import {SUCCESS_TOAST_CLASS} from "../../models/toasts/ToastModel";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit {
  images: CarouselModel[] = [
    {
      path: `/assets/images/parallax/archer_1400x875.jpg`,
      title: 'Open source bundle',
      description: 'Canyon is open source and provide a complete environment for servers.'
    },
    {
      path: `/assets/images/parallax/shaolin_1400x875.jpg`,
      title: 'ASP.Net 7 APIs',
      description: 'The backend for our services is built using latest technologies.'
    },
    {
      path: `/assets/images/parallax/shaolin2_1400x875.jpg`,
      title: 'Comet based game server',
      description: 'Game server built on top of amazing architecture created by Spirited.'
    },
    {
      path: `/assets/images/parallax/trojan1_1400x875.jpg`,
      title: 'Angular 16 frontend',
      description: 'This website is provided with the bundle and is built with latest Angular.'
    },
    {
      path: `/assets/images/parallax/warnin1_1400x875.jpg`,
      title: 'Build your own server',
      description: 'Visit cooldown.dev to see our projects and start your own server.'
    },
  ];

  newsPanels: NewsIndexModel[] = [
    {
      category: 'News',
      title: 'News example',
      content: '<p>Hello Conqueror!</p><p>Do you know that this is an HTML news example?</p>',
      author: 'Felipe',
      date: new Date('2023-10-10T20:00:00')
    },
    {
      category: 'News',
      title: 'News example',
      content: '<p style="color: darkblue; font-weight: bold;">Hello Conqueror!</p><p>Do you know that this is an HTML news example?</p>',
      author: 'Felipe',
      date: new Date('2023-10-09T20:00:00')
    },
    {
      category: 'News',
      title: 'News example',
      content: '<p>Hello Conqueror!</p><p>Do you know that this is an HTML news example?</p>',
      author: 'Felipe',
      date: new Date('2023-10-08T20:00:00')
    }
  ];

  constructor(public sanitizer: DomSanitizer,
              public toastService: ToastsService) {
  }

  ngOnInit(): void {
    // do nothing yet
  }

  showToast(): void {
    this.toastService.show(`I'm a test toast! :D`, {
      title: 'I am the title!!! ',
      className: SUCCESS_TOAST_CLASS
    });
  }
}
