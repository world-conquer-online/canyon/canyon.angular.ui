import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {catchError, map, Observable, throwError} from "rxjs";
import {environment} from "../../environments/environment";
import {CacheService} from "../services/cache.service";
import {TokenStorageModel} from "../models/authentication/token-storage-model";
import {AUTHENTICATION_CACHE_KEY} from "../services/authentication.service";
import {ToastsService} from "../services/toasts.service";
import {DANGER_TOAST_CLASS} from "../models/toasts/ToastModel";

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService implements HttpInterceptor {

  constructor(private toastService: ToastsService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.startsWith(environment.backend.url)) {
      return this.executeRequest(req, next);
    }

    const authenticationInformation = CacheService.find<TokenStorageModel>(AUTHENTICATION_CACHE_KEY);
    if (authenticationInformation) {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${authenticationInformation.accessToken}`)
      });
    }

    return this.executeRequest(req, next);
  }

  private executeRequest(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let message: string;
        if (req.url.startsWith(environment.backend.url)) {
          // our error
          message = error.error.error;

          if (error.status === 401) {
            // clear authentication cache
            CacheService.cleanLocalStorage();
          }
        }
        else {
          message = error.message;
        }

        this.toastService.show(message, {
          className: DANGER_TOAST_CLASS
        });

        if (!environment.isProduction) {
          console.log('------ Intercepted Error ------');
          console.log(JSON.stringify(error));
          console.log('------ Intercepted Error ------');
        }

        return throwError(() => error);
      }));
  }
}
